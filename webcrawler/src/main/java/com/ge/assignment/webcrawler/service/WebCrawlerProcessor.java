package com.ge.assignment.webcrawler.service;

import com.ge.assignment.webcrawler.common.Request;
import com.ge.assignment.webcrawler.common.Response;

import java.util.LinkedList;


public class WebCrawlerProcessor {

    /**
     * main method to execute the web crawler application
     * @param args
     * @throws Exception
     */
    public static void main(String [] args) throws Exception {

        Request request = RequestProcessor.getJsonData("internet_3.json");
        Response response = new Response();

        LinkedList<String> links = new LinkedList<>();

        String startingPage = "http://foo.bar.com/p1";
        RequestProcessor.pagesQueueProcessor(startingPage,request, response, links);
    }
}
