package com.ge.assignment.webcrawler.common;

import java.util.Set;

public class Response {

    private Set<String> success;
    private Set<String> skipped;
    private Set<String> error;

    public Set<String> getSuccess() {
        return success;
    }

    public void setSuccess(Set<String> success) {
        this.success = success;
    }

    public Set<String> getSkipped() {
        return skipped;
    }

    public void setSkipped(Set<String> skipped) {
        this.skipped = skipped;
    }

    public Set<String> getError() {
        return error;
    }

    public void setError(Set<String> error) {
        this.error = error;
    }
}
