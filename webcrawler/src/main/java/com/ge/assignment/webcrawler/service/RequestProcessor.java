package com.ge.assignment.webcrawler.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ge.assignment.webcrawler.common.Pages;
import com.ge.assignment.webcrawler.common.Request;
import com.ge.assignment.webcrawler.common.Response;
import org.apache.el.stream.Stream;

import java.io.InputStream;
import java.util.*;

public class RequestProcessor {

    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * static method to read Json file and return Request object
     * @param fileName
     * @return Request
     * @throws Exception
     */
    public static Request getJsonData(String fileName) throws Exception
    {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(fileName);
        Request request = mapper.readValue(inputStream, Request.class);

        return request;
    }


    /**
     * <h1>pagesQueueProcessor</h1> method to process all pages in the request and classify all pages
     * links using BFS algorithm
     * @param startingPage
     * @param request
     * @param links
     * @throws Exception
     */
    public static  void pagesQueueProcessor(String startingPage,Request request, Response response,
                                            LinkedList<String> links) throws Exception {

        Set<String> success = new HashSet<>();
        Set<String> skipped = new HashSet<>();
        Set<String> error = new HashSet<>();
        List<Pages> pages = request.getPages();


        links.add(startingPage);

        while(links.size()>0)
        {
            String link = links.get(0);
            linksChecksAndClassification(pages,link,success,skipped,error);

            links.poll();

            if(!skipped.contains(link))
                addAddressLinks(link,links,pages);

        }
        response.setSuccess(success);
        response.setSkipped(skipped);
        response.setError(error);

        printResults(response);


    }

    /**
     * <h1>addAddressLinks</h1> method to update the links Linked List with all links for the given pages
     * @param address
     * @param links
     * @param pages
     */
    private static  void addAddressLinks(String address, LinkedList<String> links,List<Pages> pages)
    {

        //Instant start= Instance.now

           pages.parallelStream().forEach(page ->{
            if(address.equalsIgnoreCase(page.getAddress()))
            {

                page.getLinks().forEach(link->{links.add(link);});
            }
        });
    }

    /**
     * <h1>linksChecksAndClassification</h1> method to check all pages links and store it in the right classification
     * @param pages
     * @param link
     * @param success
     * @param skipped
     * @param error
     */
    private static void linksChecksAndClassification(List<Pages> pages, String link, Set<String> success, Set<String> skipped, Set<String> error)
    {
        if(success.contains(link))
        {
            skipped.add(link);
        }
        else
        {
            if(isValidLink(pages,link))
                success.add(link);
            else
                error.add(link);
        }
    }

    /**
     * <h1>isValidLink</h1> method to check if the link is valid by checking if the link contains in any pages addresses
     * @param pages
     * @param link
     * @return boolean
     */
    private static boolean isValidLink(List<Pages> pages, String link)
    {
///TODO stream
        for (Pages page : pages)
        {
            if(page.getAddress().equalsIgnoreCase(link))
                return true;
        }

        return false;
    }

    /**
     * <h1>printResults</h1> method to map the Respons object to Json string and print the results
     * @param response
     * @throws JsonProcessingException
     */
    public static void printResults(Response response) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(response);
        System.out.print(json);
    }
}
