package com.ge.assignment.webcrawler.common;

import java.util.List;

public class Request {

    private List<Pages> pages;


    public List<Pages> getPages() {
        return pages;
    }

    public void setPages(List<Pages> pages) {
        this.pages = pages;
    }
}
