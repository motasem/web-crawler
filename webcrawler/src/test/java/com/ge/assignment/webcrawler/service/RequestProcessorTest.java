package com.ge.assignment.webcrawler.service;


import com.ge.assignment.webcrawler.common.Pages;
import com.ge.assignment.webcrawler.common.Request;
import com.ge.assignment.webcrawler.common.Response;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RequestProcessorTest {


    @Test
    public void getJsonDataTest() throws Exception {
        Request request = RequestProcessor.getJsonData("internet_2.json");
        Assert.assertEquals("http://foo.bar.com/p1",request.getPages().get(0).getAddress());
    }

    @Test
    public void pagesQueueProcessorTest() throws Exception {//532,580,615 --522,530,549
         List<String> links= new ArrayList<>();
         links.add("http://foo.bar.com/p2");
        links.add( "http://foo.bar.com/p3");
        links.add("http://foo.bar.com/p4");

        List<String> links1= new ArrayList<>();
        links1.add("http://foo.bar.com/p2");
        links1.add( "http://foo.bar.com/p4");

        Pages pages = new Pages();
        pages.setAddress("http://foo.bar.com/p99999999");
        pages.setLinks(links);

        Pages pages1 = new Pages();
        pages1.setAddress("http://foo.bar.com/p2");
        pages1.setLinks(links1);

        List<Pages> pagesList = new ArrayList<>();
        pagesList.add(pages);
        pagesList.add(pages1);

        Request request =new Request();
        request.setPages(pagesList);

        LinkedList<String> linksQueue = new LinkedList<>();
        Response response = new Response();

        RequestProcessor.pagesQueueProcessor("http://foo.bar.com/p99999999",request,response,linksQueue);

        Assert.assertTrue(response.getSuccess().contains("http://foo.bar.com/p99999999"));
        Assert.assertTrue(response.getSuccess().contains("http://foo.bar.com/p2"));

        Assert.assertTrue(response.getSkipped().contains("http://foo.bar.com/p2"));

        Assert.assertTrue(response.getError().contains("http://foo.bar.com/p3"));
        Assert.assertTrue(response.getError().contains("http://foo.bar.com/p4"));
    }

}
